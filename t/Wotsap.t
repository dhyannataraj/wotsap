use strict ;
use warnings ;

use lib 'lib' ;

# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl Wotsap.t'

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use Test::More qw(no_plan) ;
use Digest::MD5 ;
BEGIN { use_ok ( 'Wotsap' ) } ;

unlink sprintf "%s/%s", Wotsap::FS_TEMP, Wotsap::FS_LITE ;

#########################

# Insert your test code below, the Test::More module is use()ed here so read
# its man page ( perldoc Test::More ) for help writing this test script.

# my $FQDN = 'pgp.surfnet.nl' ;
# my $FQDN = 'pgp.mit.edu' ;
my $FQDN = 'pgp.surfnet.nl' ;
my $KID1 = '526af3f61c81a288' ; # KvdH
my $SUM  = 'c019af94dc06b24d1a3b993063293f61' ;
my $KID2 = '8b962943fc243f3c' ; # HPP
my $KOD1 = 'badbadbadbadbeaf' ;
my $PATH = 'tmp' ;
my $LEVEL = 'Verbose' ;

sub TXT { 1 ; }

my $OBB ;

is ( Wotsap -> NO, '', "Wotsap has 'NO'" ) ;
ok ( Wotsap::Tab -> Make () -> can ( 'name' ), "a Tab can do 'name'" ) ;
ok ( Wotsap::Keys -> Make () -> can ( 'cols' ), "a Keys can do 'cols'" ) ;
ok $OBB = OBB -> Make, 'make an OBB' ;
ok $OBB = OBB -> can ( 'Terse' ), 'OBB can Terse' ;
is ( OBB::_VLVL ( 'Quiet' )
   , 2
   , 'OBB level Quiet = 2'
   ) ;
is ( OBB -> Verbosity
   , OBB::_VLVL ( 'Terse' )
   , 'OBB Verbosity is Terse'
   ) ;
is ( Wotsap -> Verbosity
   , OBB::_VLVL ( 'Terse' )
   , 'Wotsap Verbosity is Terse'
   ) ;
is ( OBB -> Verbosity ( 'Quiet' )
   , OBB::_VLVL ( 'Terse' )
   , 'OBB -> Quiet'
   ) ;
is ( OBB -> Verbosity
   , OBB::_VLVL ( 'Quiet' )
   , 'OBB == Quiet'
   ) ;
my $verb = OBB -> Verbosity ;
my $nlvl = OBB::_VLVL ( 'Verbose' ) ;
is ( OBB -> Verbosity ( $nlvl )
   , $verb
   , "OBB -> level $nlvl"
   ) ;
is ( OBB -> Verbosity
   , $nlvl
   , "OBB == level $nlvl"
   ) ;

OBB -> Verbosity ( $LEVEL ) ;

my $SRV ;
ok $SRV = Wotsap::Keyserver -> Make ( fqdn => $FQDN ), 'make SRV' ;
is $SRV -> fqdn, $FQDN, 'get FQDN' ;

ok ( Wotsap::Key -> can ( 'wid' ), "a Wotsap::Key can do 'wid'" ) ;

my $W ;
ok ( $W = Wotsap -> Make
       ( path    => $PATH
       , kserver => $FQDN
       , db_type => Wotsap::DB_EXPT
       , version => '0.3'
       )
   , 'make Wotsap'
   ) ;
ok ( $W -> can ( 'keys' ), "a \$W can do 'keys'" ) ;
ok ( $W -> meta -> can ( 'version' ), "a \$W -> meta can do 'version'" ) ;
ok ( $W -> meta -> can ( 'db_type' ), "a \$W -> meta can do 'db_type'" ) ;
is ref ( $W -> keys ), 'Wotsap::Keys', "type \$W -> keys" ;
is ( scalar @{ $W -> keys -> cols}, 0, "\$W -> keys -> cols" ) ;
is ( $W -> meta -> count, 1, "\$W meta count == 1" ) ;
is ( $W -> meta -> version, '0.3', "\$W get_version 0.3" ) ;
is ( $W -> meta -> version ( 'xxx' ), 'xxx', "\$W set_version xxx" ) ;
is ( $W -> meta -> version ( '0.3' ), '0.3', "\$W set_version 0.3" ) ;

my $KEY1 ;
my $BLK ;
ok ( $KEY1 = $W -> mk_key ( $KID1 )
   , "make key1 $KID1"
   ) ;
is ( $KEY1 -> kid
   , lc $KID1
   , "get  key1 $KID1"
   ) ;
is ( $KEY1 -> def_sum
   , 'BLCINIT000000000' . lc $KID1
   , "def_sum key1 $KID1"
   ) ;

is $BLK = $SRV -> get_key ( $KOD1 ) -> blc, '' , 'no block for BAD key' ;
my $BAD_srv = Wotsap::Keyserver -> Make ( fqdn => $FQDN . 'x' ) ;
is $BAD_srv -> get_key ( $KID1 ) -> blc, '', 'no block for BAD server' ;

ok $BLK = $SRV -> get_key ( $KID1 ) -> blc , "get block key1 0x$KID1" ;
my $hex = Digest::MD5 -> new -> add ( $BLK ) -> hexdigest ;
is $hex, $SUM, "ok sum for block key1 $KID1" ;
is $KEY1 -> sum ( $SUM ), $SUM, "get/set sum key1 $KID1" ;
is $KEY1 -> bad ( 'xx' ), 'xx', "get/set bad key1 $KID1" ;

my $tmp ;

is $W -> key  ( $KID1 ), undef, "not in store key1 $KID1" ;
is $W -> save ( $KEY1, {} ), 1, "save key1 $KID1" ;
is $W -> key  ( $KID1 ) -> kid, $KID1, "key1 $KID1 in store" ;
is $W -> key  ( $KID1 ) -> sum, $SUM, "key1 $KID1 has sum $SUM" ;
is $W -> drop_keys ( $KEY1 ), 1, "store drop key1 $KID1" ;
is $W -> key  ( $KID1 ), undef, "key1 $KID1 not in store" ;

$KEY1 = $W -> mk_key ( $KID1 ) ;

ok $W -> serv, "\$W has keyserver" ;
is $W -> refresh ( $KEY1 ) -> upd, 'upd', "refresh key1 $KID1" ;
is $W -> key ( $KID1 ) -> kid, $KEY1 -> kid, "key1 $KID1" ;
is $W -> key ( $KID1 ) -> done, undef, "key1 done == undef" ;
$W -> key ( $KID1 ) -> mark_as_todo ;
is $W -> key ( $KID1 ) -> done,     0, "key1 done == 0" ;
$W -> key ( $KID1 ) -> mark_as_done ;
ok $W -> key ( $KID1 ) -> done, "key1 done > 0" ;

my @sigs = $W -> signatures ( $KEY1 ) ;
is scalar @sigs, 35, "count sigs key2 $KID2" ;
my $KEY2 = $W -> key ( $KID2 ) ;
is $W -> signed ( $KEY2, $KEY1 ), 7, "key2 $KID2 signed key1 $KID1" ;

$KEY2 = $W -> key ( $KID2 ) || $W -> mk_key ( $KID2 ) ;
is $W -> refresh ( $KEY2 ) -> upd, 'new', "refresh key2 $KID2" ;
is $W -> key ( $KID2 ) -> kid, $KEY2 -> kid, "key2 $KID2" ;
ok $W -> key ( $KID2 ) -> id, "have some id key2 $KID2\n" ;
is $W -> refresh ( $KEY2 ) -> upd, '', "refresh key2 $KID2 again" ;
ok $W -> keys -> save ( $KEY1 ), 'save again' ;

@sigs = $W -> signatures ( $KEY2 ) ;
is scalar @sigs, 150, "count sigs key2 $KID2" ;
is ( $W -> sigs -> count ( where => "dst = '$KEY2->{id}'" )
   , 150
   , "sigs -> count key2 $KID2"
   ) ;

my $KEY_bad = $W -> mk_key ( $KOD1 ) ;
is $W -> refresh ( $KEY_bad ) -> upd, 'emp', "refresh bad $KOD1" ;
my $sum_empty = Wotsap::Result -> Make ( kid => $KOD1, blc => '' ) -> sum ;
is $W -> key ( $KOD1 ) -> sum, $sum_empty, "sum bad $KOD1 == $sum_empty" ;

my $NOPR = '--no-print-directory' ;
system ( "cd src/graph ; rm -f graph ; make -s $NOPR graph" ) ; #ignore errors
SKIP :
  { skip "can't make src/graph/graph", 2 unless -f 'src/graph/graph' ;
    unlink 'src/graph/res.test1' ; # ignore errors
    system "cd src/graph/ ; make -s $NOPR t_test1" ;
    ok ( -f 'src/graph/res.test1' , "graph null" ) ;
    unlink 'src/graph/res.test2' ; # ignore errors
    system "cd src/graph/ ; make -s $NOPR t_test2" ;
    ok ( -f 'src/graph/res.test2' , "graph S3" ) ;
  }
