=pod

=head1 NAME

  wotsap - Wotsap archive manipulation

=head1 SYNOPSIS

  Usage: wotsap [-v] [-q] [-d] [-h] work-dir COMMAND ...
  option v : be verbose
  option q : be quiet
  option d : show debug info
  option h : show help ; exit
  ---------------------------
  COMMANDS:
  Usage: make [-g path/to/gpg] [-k key_server] [-r root_key]
  Usage: open
  Usage: info [-v] [key_id]
  Usage: import wotsap-file
  Usage: check
  Usage: reset [-keysum where] [-keybad where]
  Usage: update [-cache days] max [kid]
  Usage: scc
  Usage: export [-v version]
  Usage: xpaths [-j] [-r] [-m max] from to
  

=head1 DESCRIPTION

Program B<wotsap> handles Wotsap archives.
A Wotsap archive is a densely packed file containing
historic information about the I<PGP strong set>.
See L<http://pgp.cs.uu.nl/doc/> for a description of the Wotfile format.

Central to B<wotsap> is the notion of a I<Wotsap working directory>
(I<workdir> for short). A workdir is just a directory containing
files pertaining to one Wotsap archive.

To decompress, unpack and decode an existing Wotsap archive I<my-archive>
into workdir I<my-workdir> use :

  wotsap my-workdir make import my-archive

To create a fresh Wotsap archive C<prod/export.wot>, use :

  wotsap prod make -k some-key-server update -reset 0 scc export

... and to refresh :

  wotsap prod update -reset 0 scc export

B<Please note> :

=over

=item *

At this moment, a full update run retrieves some 100.000 keys from
a keyserver ; unless you own the key-server, ask permission first!

=back

=head1 Options

=over 4

=item -v

be verbose

=item -q

be quiet

=item -d

show debug info

=item -h

show help ; exit

=back

=head1 Commands

=head2 command I<make>

  Usage: make [-g path/to/gpg] [-k key-server] [-r root-key]

Command I<make> creates or updates a workdir.

The values of the options are stored in the workdir as meta-information.

=over 4

=item B<-g> I<pgp-path>

The path to program B<gpg>(1), default C</usr/bin/gpg>.

=item B<-k> I<key-server>

The domain name of a pgp keyserver ; no default.

=item B<-r> I<root-key>

The start-key used by commands C<update> and C<scc> ;
default C<8b962943fc243f3c>.

=back

=head2 command I<open>

  Usage: open

Command I<open> just execs sqlite3(1) on the workdir's sqlite database ;
for your convenience.

=head2 command I<info>

  Usage: info [-v] [key-id]

Command I<info> shows the meta-data stored in the workdir's database.

If a I<key-id>] is provided, some info about the key is shown ;
otherwise the number of keys and signatures is shown.

=head2 command I<import>

  Usage: import wotsap-file

=over 4

=item *

Command I<import> copies the specified I<wotsap-file> into the workdir
as C<import.wot>.

=item *

Then, it decompresses and unpacks I<wotsap-file>, creating files
C<README>, C<WOTVERSION>, C<names>, C<keys>, C<signatures> and C<debug>.

=item *

Then, it decodes files C<keys> and C<signatures> into 
files C<keys.txt> and C<signatures.txt>.

=item *

Then, it creates a fresh sqlite database C<wotsap.lite>
with the proper layout and meta-info,
and inserts the imported keys and signature information.

=item *

Then, it verifies that the keys+signatures form a graph that is
I<strongly connected> (contains exactly one I<strongly connected component>).

=item *

Files C<keys.txt>, C<names> and C<signatures.txt> have the same number
of lines ; corresponding lines refer to the same key.

=back

=head2 command I<check>

  Usage: check

Command I<check> verifies that the keys and signatures in the workdir's
sqlite database form a I<strongly connected graph>.

=head2 command I<reset>

  Usage: reset [-keydata where]

Command I<reset> resets the C<update> process :
the queue is emptied and all keys are set to I<not visited>.

Option I<-keydata> resets meta-data for keys
specified with the I<where>-clause.

On subsequent C<update> runs, the keys will be
re-fetched from the key-server (or the cache),
and re-evaluated.

Example

  reset -keydata "bad = ''"
  reset -keydata "bad = 'expired'"
  reset -keydata "kid = 'some-key-id'"

=head2 command I<update>

  Usage: update [-cache days] max [kid]

Command I<update> fetches keys from a keyserver (or the cache),
and updates the workdir's database.

At most $max keys are updated ; C<$max = 0> means I<no limit>.

Starting with some I<root-key>, command I<update> does a
search of the PGP graph, updating and marking visited keys.

  queue = [ some-root-key ] # if no keys are marked 'visited'

  while ( queue-is-not-empty )
    { key = shift queue ;
      if ( key.visited == 0 )
        { update key ;
          key.visited = 1 ;
          push queue key.signers ;
        }
    }

Command I<update> remembers the contents of the I<queue>
and the state of the keys (visited, not-visited) from
one run to the next ; it continues where the previous
I<update>-run stopped.

If an optional key_id I<kid> is supplied, that key is deleted,
and used as I<some-root-key> ; I<max> is set to I<1>.
This is for debugging :

  % wotsap -d X update 1 kid

With option I<-cache $days>, fetched keys are cached,
and cached keys will be used (if not older that $days days),
instead of fetched from the key-server.

Keys that were not found on the keyserver in a previous update-run
will always be retrieved from the keyserver,
because only non-empty key-blocks are cached.

With I<$days = 0>, the cache will be written but not used.

Caching is cheap in cpu-cycles (involving only file link/unlinks),
but a cache typically uses 1.2 GB per 100,000 keys.

=head2 command I<scc>

  Usage: scc

Command I<scc> computes the SCC of the keys and signatures
in the workdir's database, marking keys with a I<wid>-bit :
I<in> or I<not in> the strong set.

=head2 command I<export>

  Usage: export [version]

Command I<export> creates an export file C<export.wot>.

Optionally specify the export file's Wotsap version : C<0.2> or C<0.3> ;
default is C<0.3>.

=head2 command I<xpaths>

  Usage: xpaths [-j] [-r] [-m max] from to

Command I<xpaths> finds paths from key I<from> to key I<to>.

=over 4

=item -j

Print a json identifying I<from>, I<to> and the
(interior of the) paths found.

=item -r

Find reverse paths.

=item -m I<max>

Return at most I<max> paths ; default 8.

=back

Regarding arguments I<from> and I<to> :

=over 4

=item *

Key I<from> and key I<to> must be at least 8 hex digits long.

=item *

Key I<from> and/or key I<to> may be specified as C<root>.

=item *

If key I<from> or key I<to> is longer than the workdir's key length
(8 for wotsap version 0.1 and 0.2, 16 for version 0.3) a proper suffix
of the key is used.

=item *

If key I<from> or key I<to> is shorter than the workdir's key length,
some key with the given suffix is used.

=back

=head1 INSTALL

Wotsap is available from L<http://pgp.cs.uu.nl/wotsap/>.

Install with

  cpanm http://pgp.cs.uu.nl/wotsap/Wotsap.tar.gz

or, test only with

  cpanm --test-only http://pgp.cs.uu.nl/wotsap/Wotsap.tar.gz

=head1 SEE ALSO

=for html <a href="Wotsap.html">Wotsap(3)</a>

=for man Wotsap(3)

=head1 AUTHOR

=for html Wotsap &copy; 2015
<a href="http://www.staff.science.uu.nl/~penni101">Henk P. Penning</a>
- All rights reserved
; Wotsap-0.02.09 - Fri Aug 24 16:35:48 2018

=for man Wotsap by
Henk P. Penning, <http://www.staff.science.uu.nl/~penni101>.
- Wotsap-0.02.09 - Fri Aug 24 16:35:48 2018

You may distribute under the terms of either the GNU General Public
License or the Artistic License, as specified in the Perl 5.10.0 README
file.

=cut
